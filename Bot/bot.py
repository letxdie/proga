import random
from enum import Enum
from typing import Tuple
import requests
import config
import telebot
from bs4 import BeautifulSoup
import datetime as dt

bot = telebot.TeleBot(config.access_token)


class DayOfWeek(Enum):
    MONDAY = 1
    TUESDAY = 2
    WEDNESDAY = 3
    THURSDAY = 4
    FRIDAY = 5
    SATURDAY = 6
    SUNDAY = 7


@bot.message_handler(commands=['start'])
def info(message):
    bot.send_message(message.chat.id, """Hi   
    <b>Название группы писать с заглавной буквы</b>
    <b>/all GROUP </b>- Расписание на эту неделю 
    <b>/tomorow GROUP </b>- Расписание на завтра
    <b>/near GROUP </b>- Ближайшее занятие
    <b>/monday GROUP 1</b> <i>или любой другой день или
     четность недели в указанном формате.</i>""", parse_mode='HTML')


@bot.message_handler(commands=['time'])
def info(message):
    bot.send_message(message.chat.id, dt.datetime.today())


def get_page(group: str = '', week: str = '') -> str:
    if week:
        week = str(week) + '/'
    url = '{domain}/0/{group}/{week}raspisanie_zanyatiy_{group}.htm'.format(
        domain=config.domain,
        week=week,
        group=group)
    response = requests.get(url)
    web_page = response.text
    return web_page


def parse_schedule(web_page: Any, day: str) -> Tuple[list, list, list, list]:
    soup = BeautifulSoup(web_page, "html5lib")

    # Получаем таблицу с расписанием на понедельник
    schedule_table = soup.find("table", attrs={"id": day})

    # Время проведения занятий
    times_list = schedule_table.find_all("td", attrs={"class": "time"})
    times_list = [time.span.text for time in times_list]

    # Место проведения занятий
    locations_list = schedule_table.find_all("td", attrs={"class": "room"})
    locations_list = [room.span.text for room in locations_list]

    # Название дисциплин и имена преподавателей
    lessons_list = schedule_table.find_all("td", attrs={"class": "lesson"})
    lessons_list = [lesson.text.split('\n\n') for lesson in lessons_list]
    lessons_list = [', '.join([info for info in lesson_info if info]) for lesson_info in lessons_list]

    # Номер аудитории
    room_list = schedule_table.find_all("td", attrs={"class": "room"})
    room_list = [room.dd.text for room in room_list]

    return times_list, locations_list, lessons_list, room_list


@bot.message_handler(commands=['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'])
def get_schedule(message):
    """ Получить расписание на указанный день """
    day2id = {
        "monday": "1day",
        "tuesday": "2day",
        "wednesday": "3day",
        "thursday": "4day",
        "friday": "5day",
        "saturday": "6day",
        "sunday": "7day"
    }
    try:
        day, group, week = message.text.split()
        web_page = get_page(group, week)
    except ValueError:
        try:
            day, group = message.text.split()
            web_page = get_page(group)
        except ValueError:
            bot.send_message(message.chat.id, "Неправильный формат ввода")
            return None
    day = day[1:]
    day = day2id[day]

    try:
        times_lst, locations_lst, lessons_lst, room_list = parse_schedule(web_page, day)
        resp = ''
        for time, location, lession, room in zip(times_lst, locations_lst, lessons_lst, room_list):
            resp += f'<b>{time}</b>, {location}, <b>{room}</b>{lession}\n'
        bot.send_message(message.chat.id, resp, parse_mode='HTML')
    except AttributeError:
        bot.send_message(message.chat.id, 'Для данной группы нет занятий в этот день.')


def mod_week() -> bool:
    week = dt.datetime.today().isocalendar()[1]  # Определяем четность недели
    if week % 2 == 0:
        week_bool = True
    else:
        week_bool = False
    return week_bool


@bot.message_handler(commands=['tomorow'])
def get_tommorow(message):
    """ Получить расписание на следующий день """
    day = dt.datetime.today().isoweekday()
    if mod_week():
        week = '1'
    else:
        week = '2'
    if day == DayOfWeek.SUNDAY:
        day = DayOfWeek.MONDAY
        if mod_week():
            week = '2'
        else:
            week = '1'
    else:
        day += 1
    day_format = str(day) + 'day'
    try:
        _, group = message.text.split()
        web_page = get_page(group, week)
    except ValueError:
        bot.send_message(message.chat.id, 'Неправильный формат ввода')
        return None
    try:
        times_lst, locations_lst, lessons_lst, room_list = parse_schedule(web_page, day_format)
        resp = ''
        for time, location, lession, room in zip(times_lst, locations_lst, lessons_lst, room_list):
            resp += f'<b>{time}</b>, {location}, <b>{room}</b>{lession}\n'
        bot.send_message(message.chat.id, resp, parse_mode='HTML')
    except AttributeError:
        bot.send_message(message.chat.id, 'Для данной группы завтра нет занятий.')


@bot.message_handler(commands=['all'])
def get_all_schedule(message):
    """ Получить расписание на всю неделю для указанной группы """
    day = dt.datetime.today().isoweekday()
    if mod_week():
        week = '1'
    else:
        week = '2'
    if day == DayOfWeek.SUNDAY:
        if mod_week():
            week = '2'
        else:
            week = '1'
    day_format = ['0']
    for day in today:
        dayw = str(day) + 'day'
        day_format.append(dayw)
    try:
        _, group = message.text.split()
        web_page = get_page(group, week)
        if not schedule_exists(group):
            bot.send_message(message.chat.id, "Для данной группы нет расписания")
            return None
    except ValueError:
        bot.send_message(message.chat.id, "Неправильный формат ввода")
        return None
    for i in range(1, 8):
        today_day = today[i]
        try:
            resp = ''
            times_lst, locations_lst, lessons_lst, room_list = parse_schedule(web_page, day_format[i])
            for time, location, lession, room in zip(times_lst, locations_lst, lessons_lst, room_list, ):
                resp += f'<b>{time}</b>, {location}, <b>{room}</b>{lession}\n'
            bot.send_message(message.chat.id, today_day + resp, parse_mode='HTML')
        except AttributeError:
            bot.send_message(message.chat.id, today_day + '\n нет занятий.')
            continue


@bot.message_handler(commands=['near'])
def get_near_lesson(message):
    """ Получить ближайшее занятие """
    try:
        day = dt.datetime.today().isoweekday()
        minutes = minutes_now()
        if mod_week():
            week = '1'
        else:
            week = '2'
        _, group = message.text.split()
        if not schedule_exists(group):
            bot.send_message(message.chat.id, 'Для данной группы нет расписания')
        else:
            location, time, lesson, room, day = check(day, week, minutes, group)
            day = today[day]
            resp = day + f'<b>{time}</b>, {location}, <b>{room}</b>{lesson}\n'
            bot.send_message(message.chat.id, resp, parse_mode='HTML')

    except ValueError:
        bot.send_message(message.chat.id, 'Недопустимый формат')


def schedule_exists(group: str) -> bool:
    web_page = get_page(group, '')
    soup = BeautifulSoup(web_page, "html5lib")
    confirm = soup.find("article", attrs={"class": "content_block"})
    confirm = confirm.text
    if "Расписание не найдено" in confirm:
        return False
    else:
        return True


def minutes_now() -> int:
    hours = dt.datetime.today().time().hour
    minutes = dt.datetime.today().time().minute
    minutes_now = hours * 60 + minutes
    return minutes_now


def check(day, week: str, minutes_now: int, group: str):
    group_a = group
    day_format = str(day) + 'day'
    try:
        web_page = get_page(group, week)
        times_lst, locations_lst, lessons_lst, room_list = parse_schedule(web_page, day_format)
        count = 0
        for time in times_lst:
            time = str(time)
            try:
                index = time.index('-')
                time = time[0:index]
                index = time.index(":")
                hours = int(time[0:index])
                minutes = int(time[index + 1:])
                minutes_rasp = hours * 60 + minutes
                if minutes_now < minutes_rasp:
                    return locations_lst[count], times_lst[count], lessons_lst[count], room_list[count], day
                count += 1
                if count == len(times_lst):
                    return check(day=day + 1, week=week, minutes_now=0, group=group_a)
            except ValueError:
                if day == DayOfWeek.SUNDAY:
                    if week == '1':
                        week = '2'
                    else:
                        week = '1'
                    day = DayOfWeek.MONDAY
                    return check(day=day, week=week, minutes_now=0, group=group_a)
                else:
                    return check(day=day + 1, week=week, minutes_now=0, group=group_a)
    except AttributeError:
        if day == DayOfWeek.SUNDAY:
            day = DayOfWeek.MONDAY
            if mod_week():
                week = '2'
            else:
                week = '1'
            return check(day=day, week=week, minutes_now=0, group=group_a)
        else:
            return check(day=day + 1, week=week, minutes_now=0, group=group_a)


@bot.message_handler(content_types=['text'])
def nope(message):
    phrase = random.randint(0, 3)
    phrases = ['Прости, но я не говорю на твоем языке', 'Я не понимаю', 'Это не входит в мои обязанности', 'хмм...']
    bot.send_message(message.chat.id, phrases[phrase])


today = {
    1: "Пн ",
    2: "Вт ",
    3: "Ср ",
    4: "Чт ",
    5: "Пт ",
    6: "Сб ",
    7: "Вс ",
}

if __name__ == '__main__':
    bot.polling(none_stop=True)
