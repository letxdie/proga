import copy
from typing import Any
import pygame
from pygame.locals import *
import random


class GameOfLife:
    def __init__(self, width: int = 640, height: int = 480, cell_size: int = 10, speed: int = 10) -> None:
        self.width = width
        self.height = height
        self.cell_size = cell_size

        # Устанавливаем размер окна
        self.screen_size = width, height
        # Создание нового окна
        self.screen = pygame.display.set_mode(self.screen_size)

        # Вычисляем количество ячеек по вертикали и горизонтали
        self.cell_width = self.width // self.cell_size
        self.cell_height = self.height // self.cell_size

        # Скорость протекания игры
        self.speed = speed

    def draw_grid(self) -> None:
        """ Отрисовать сетку """
        for x in range(0, self.width, self.cell_size):
            pygame.draw.line(self.screen, pygame.Color('black'),
                             (x, 0), (x, self.height))
        for y in range(0, self.height, self.cell_size):
            pygame.draw.line(self.screen, pygame.Color('black'),
                             (0, y), (self.width, y))

    def run(self) -> None:
        pygame.init()
        clock = pygame.time.Clock()
        self.clist = self.cell_list()
        pygame.display.set_caption('Game of Life')
        self.screen.fill(pygame.Color('white'))
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == QUIT:
                    running = False
            self.draw_grid()
            self.draw_cell_list(self.clist)
            self.update_cell_list(self.clist)
            pygame.display.flip()
            clock.tick(self.speed)
        pygame.quit()

    def cell_list(self, randomize: bool = True) -> Any:
        """ Создание списка клеток.
        :param randomize: Если True, то создается список клеток, где
        каждая клетка равновероятно может быть живой (1) или мертвой (0).
        :return: Список клеток, представленный в виде матрицы
        """
        self.clist = []
        self.clist = [[0] * self.cell_width for _ in range(self.cell_height)]
        if randomize:
            for col in range(self.cell_height):
                for row in range(self.cell_width):
                    self.clist[col][row] = random.randint(0, 1)
        return self.clist

    def draw_cell_list(self, clist: list) -> None:
        """ Отображение списка клеток
        :param rects: Список клеток для отрисовки, представленный в виде матрицы
        """
        for col in range(self.cell_height):
            for row in range(self.cell_width):
                x = row * self.cell_size + 1  # Координаты левого угла по X
                y = col * self.cell_size + 1  # Координаты левого угла по Y
                width = self.cell_size - 1  # Ширина прямоугольника
                height = self.cell_size - 1  # Высота прямоугольника
                if clist[col][row]:
                    pygame.draw.rect(self.screen, pygame.Color('green'), (x, y, width, height))
                else:
                    pygame.draw.rect(self.screen, pygame.Color('white'), (x, y, width, height))

    def get_neighbours(self, cell: Any) -> list:
        """ Вернуть список соседей для указанной ячейки
        :param cell: Позиция ячейки в сетке, задается кортежем вида (row, col)
        :return: Одномерный список ячеек, смежных к ячейке cell
        """
        # PUT YOUR CODE HERE
        coll, roww = cell
        height = self.cell_height - 1
        width = self.cell_width - 1
        neighbours = [self.clist[col][row] for col in range(coll - 1, coll + 2) for row in range(roww - 1, roww + 2) if
                      (0 <= col <= height) and (0 <= row <= width) and (col != coll or row != roww)]
        return neighbours

    def update_cell_list(self, cell_list: Any) -> Any:
        """ Выполнить один шаг игры.
        Обновление всех ячеек происходит одновременно. Функция возвращает
        новое игровое поле.
        :param cell_list: Игровое поле, представленное в виде матрицы
        :return: Обновленное игровое поле
        """
        self_clist = copy.deepcopy(self.clist)
        for col in range(self.cell_height):
            for row in range(self.cell_width):
                neighbours = sum(self.get_neighbours((col, row)))
                if self.clist[col][row]:
                    if not 1 < neighbours < 4:
                        self_clist[col][row] = 0
                else:
                    if neighbours == 3:
                        self_clist[col][row] = 1
        self.clist = self_clist
        return self.clist


if __name__ == '__main__':
    game = GameOfLife(320, 240, 20)
    game.run()
