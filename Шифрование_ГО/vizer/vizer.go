package vizer

func EncryptVigenere(plaintext string, keyword string) string {
	var a int
	var b int
	var ciphertext string
	var count  = 0
	for i := 0; i < len(plaintext); i++{
		b = int(keyword[count])
		a = int(plaintext[i])
		if b < 96{
			b -= 65
		}else {
			b -= 97
		}
		if a < 91 && a + b > 90{
			a += b - 26
		} else if a < 123 && a + b > 122 {
			a += b - 26
		}else {
			a += b
		}
		count += 1
		if count == len(keyword){
			count = 0
		}
		ciphertext += string(a)
		}
	return ciphertext
}

func DecryptVigenere(ciphertext string, keyword string) string {
	var plaintext string
	var a int
	var b int
	var count  = 0
	for i := 0; i < len(ciphertext); i++{
		b = int(keyword[count])
		a = int(ciphertext[i])
		if b < 96{
			b -= 65
		}else {
			b -= 97
		}
		if a > 64 && a - b < 65{
			a = a - b + 26
		} else if a > 96 && a - b < 97 {
			a = a -  b + 26
		}else {
			a -= b
		}
		count += 1
		if count == len(keyword){
			count = 0
		}
		plaintext += string(a)
	}
	return plaintext
}