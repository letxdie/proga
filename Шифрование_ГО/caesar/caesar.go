package caesar

func EncryptCaesar(plaintext string, num int) string {
	var i_s int
	var s_s = ""
	for i := 0; i < (len(plaintext)) ; i++ {
		i_s = int(plaintext[i])
		if i_s < 91 && i_s > 64 {
			if i_s + num > 90 {
				i_s -= 26-num
				s_s += string(i_s)
			}else {s_s += string(i_s + num)}
		}else if i_s < 123 && i_s > 96 {
			if i_s+num > 122 {
				i_s -= 26-num
				s_s += string(i_s)
			}else {s_s += string(i_s + num)}
		}else{s_s += string(i_s)}
	}
	return s_s
}

func DecryptCaesar(ciphertext string, num int) string  {
	var i_s int
	var s_s = ""
	for i := 0; i < (len(ciphertext)) ; i++ {
		i_s = int(ciphertext[i])
		if i_s < 91 && i_s > 64 {
			if i_s - num < 65 {
				i_s += 26 - num
				s_s += string(i_s)
			}else {s_s += string(i_s - num)}
		}else if i_s < 123 && i_s > 96 {
			if i_s - num < 97 {
				i_s += 26 - num
				s_s += string(i_s)
			}else {s_s += string(i_s - num)}
		}else{s_s += string(i_s)}
	}
	return s_s
}
