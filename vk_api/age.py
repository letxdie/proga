import statistics
from typing import Any
import datetime
from api import get_friends


def age_predict(user_id: int) -> Any:
    """ Наивный прогноз возраста по возрасту друзей
    Возраст считается как медиана среди возраста всех друзей пользователя
    :param user_id: идентификатор пользователя
    """
    assert isinstance(user_id, int), "user_id must be positive integer"
    assert user_id > 0, "user_id must be positive integer"
    # PUT YOUR CODE HERE
    mass_age = []
    resp = get_friends(user_id, "bdate")
    if resp:
        for i in resp:
            if "bdate" in i:
                string = i['bdate']
                if len(string) > 5:
                    year_s = string[-4:]
                    string = string[0:-5]
                    point_index = string.index(".")
                    day_s = string[0:point_index]
                    month_s = string[point_index + 1:]
                    user_date = datetime.date(int(year_s), int(month_s), int(day_s))
                    now_date = datetime.date.today()
                    delta = (now_date - user_date).days
                    age = delta // 365.25
                    mass_age.append(age)
        if mass_age:
            return statistics.median(mass_age)
