import datetime
import time
from typing import Any, Tuple, Union, List
import igraph
import config
import requests
from collections import Counter
import plotly
import plotly.offline
import plotly.graph_objs as go
from igraph import Graph, plot


def get(url: str, params: dict = {}, timeout: int = 5, max_retries: int = 5,
        backoff_factor:float = 0.3) -> requests.models.Response:
    """ Выполнить GET-запрос
    :param url: адрес, на который необходимо выполнить запрос
    :param params: параметры запроса
    :param timeout: максимальное время ожидания ответа от сервера
    :param max_retries: максимальное число повторных запросов
    :param backoff_factor: коэффициент экспоненциального нарастания задержки
    """
    for retry in range(max_retries):
        try:
            response = requests.get(url, params=params, timeout=timeout)
            return response
        except requests.exceptions.RequestException:
            if retry == max_retries - 1:
                raise
            backoff = backoff_factor * (2 ** retry)
            print(backoff) #just for beauty
            time.sleep(backoff)


def get_friends(user_id: int = 168019363, fields: str = "") -> Any:
    """ Вернуть данных о друзьях пользователя
    :param user_id: идентификатор пользователя, список друзей которого нужно получить
    :param fields: список полей, которые нужно получить для каждого пользователя
    """
    assert isinstance(user_id, int), "user_id must be positive integer"
    assert isinstance(fields, str), "fields must be string"
    assert user_id > 0, "user_id must be positive integer"
    # PUT YOUR CODE HERE
    query_params = {
        'domain': "https://api.vk.com/method",
        'access_token': config.vk['tok'],
        'user_id': user_id,
        'fields': fields,
        'v': '5.87'
    }
    url = "{}/friends.get".format(query_params['domain'])
    response = get(url, params=query_params)
    time.sleep(0.34)
    return response.json()['response']['items']


def messages_get_history(user_id: int, offset: int = 0, count: int = 1000) -> Any:
    assert isinstance(user_id, int), "user_id must be positive integer"
    assert user_id > 0, "user_id must be positive integer"
    assert isinstance(offset, int), "offset must be positive integer"
    assert offset >= 0, "user_id must be positive integer"
    assert count >= 0, "user_id must be positive integer"
    mas: list = []
    query_params = {
        'domain': "https://api.vk.com/method",
        'access_token': config.vk['tok'],
        'user_id': user_id,
        'fields': "",
        'v': '5.87',
        "count": count,
        "offset": offset
    }

    if count > 200:
        div, mod = divmod(count, 200)
        query_params['count'] = 200
        for i in range(div):
            url = "{}/messages.getHistory".format(query_params['domain'])
            response = get(url, params=query_params)
            time.sleep(0.34)
            mas.extend(response.json()['response']['items'])
            query_params['offset'] = 200 * (i + 1)
            if i == div - 1 and mod != 0:
                query_params['count'] = mod
                url = "{}/messages.getHistory".format(query_params['domain'])
                response = get(url, params=query_params)
                mas.extend(response.json()['response']['items'])
        return count_dates_from_messages(mas)

    else:
        url = "{}/messages.getHistory".format(query_params['domain'])
        response = get(url, params=query_params)
        return count_dates_from_messages(response.json()['response']['items'])


def count_dates_from_messages(messages: Any) -> Tuple[list, list]:
    dates = [datetime.datetime.fromtimestamp(messages[i]['date']).strftime("%Y-%m-%d")
             for i in range(len(messages))]
    stat = Counter(dates)
    x = [date for date in stat]
    y = [stat[date] for date in stat]
    return x, y


def stat() -> None:
    plotly.tools.set_credentials_file(username='letxdie', api_key='imb361ropeIuJbJKCH3c')
    x, y = messages_get_history(140040982, 0, 600)
    plotly.offline.plot({"data": [go.Scatter(x=x, y=y)],
                         "layout": go.Layout(title="История активности переписки")},
                        image='jpeg', image_filename='stat')


def get_network(users_ids: list, as_edgelist: bool = True) -> Union[List[List[int]], List[Tuple[int, int]]]:
    edgelist = []
    network = [[0] * len(users_ids) for _ in range(len(users_ids))]
    users = users_ids
    for user in users:
        try:
            friend_list = get_friends(user)
        except KeyError:
            continue

        for friend in friend_list:
            if friend in users:
                edgelist.append((users.index(user), users.index(friend)))
                network[users.index(user)][users.index(friend)] = 1

    if as_edgelist:
        return edgelist
    return network


def name_list() -> list:
    name = []
    n = get_friends(fields="name")
    for i in n:
        name.append(i['last_name'])
    return name


def plot_graph(edge_list: list, name_list: list = []) -> None:
    vertices = name_list
    g = Graph(vertex_attrs={"label": vertices},
              edges=edge_list, directed=False)
    g.simplify(multiple=True, loops=True)
    N = len(vertices)
    visual_style = {
        "vertex_size": 20,
        "bbox": (1500, 1500),
        "margin": 200,
        "vertex_label_dist": 1,
        "edge_color": "grey",
        "layout": g.layout_fruchterman_reingold(
            maxiter=10000,
            area=N ** 2,
            repulserad=N ** 2),

    }

    clusters = g.community_multilevel()
    pal = igraph.drawing.colors.ClusterColoringPalette(len(clusters))
    g.vs['color'] = pal.get_many(clusters.membership)
    plot(g, **visual_style)
