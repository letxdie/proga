
def encrypt_caesar(plaintext: str, num=3)->str:
    """
        >>> encrypt_caesar("PYTHON")
        'SBWKRQ'
        >>> encrypt_caesar("python")
        'sbwkrq'
        >>> encrypt_caesar("Python3.6")
        'Sbwkrq3.6'
        >>> encrypt_caesar("")
        ''
        """
    new_stroka:str = ''
    for x in plaintext:
        char:int = ord(x)
        if 64 < char < 91:
            if char + num > 90:
                new_stroka += chr((char + num - 90) + 64)
            else:
                new_stroka += chr(char + num)
        elif 96 < char < 123:
            if char + 3 > 122:
                new_stroka += chr((char + num - 122) + 96)
            else:
                new_stroka += chr(char + num)
        else:
            new_stroka += x
    return new_stroka


def decrypt_caesar(ciphertext: str, num=3)->str:
    """
        >>> decrypt_caesar("SBWKRQ")
        'PYTHON'
        >>> decrypt_caesar("sbwkrq")
        'python'
        >>> decrypt_caesar("Sbwkrq3.6")
        'Python3.6'
        >>> decrypt_caesar("")
        ''
        """
    new_stroka:str = ''
    for x in ciphertext:
        char:int = ord(x)
        if 64 < char < 91:
            if char - num < 65:
                new_stroka += chr(91 - (65 - char + num))
            else:
                new_stroka += chr(char - num)
        elif 96 < char < 123:
            if char - num < 97:
                new_stroka += chr(123 - (97 - char + num))
            else:
                new_stroka += chr(char - num)
        else:
            new_stroka += x
    return new_stroka
