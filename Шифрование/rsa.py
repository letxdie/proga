from random import random


def is_prime(n: int)-> bool:
    """
       >>> is_prime(2)
       True
       >>> is_prime(11)
       True
       >>> is_prime(8)
       False
       """
    if n < 2:
        return False
    for i in range(2, n):
        if not n % i:
            return False
    return True


def gcd(a: int, b: int) -> int:
    """
    >>> gcd(12, 15)
    3
    >>> gcd(3, 7)
    1
    """
    while a != b:
        if a > b:
            a = a - b
        else:
            b = b - a
    return a


def multiplicative_inverse(e: int, phi: int) -> int:
    """
    >>> multiplicative_inverse(7, 40)
    23
    """
    mass = []
    while True:
        mass.append([phi, e, phi % e, phi // e])
        phi, e = e, mass[-1][2]
        if mass[-1][2] == 0:
            break
    mass[-1].extend([0, 1])
    for i in range(len(mass) - 2, -1, -1):
        x = mass[i + 1][-1]
        y = mass[i + 1][-2] - mass[i + 1][-1] * mass[i][3]
        mass[i].extend([x, y])
    d = mass[0][-1] % mass[0][0]
    return d


def generate_keypair(p: int, q: int)->tuple:
    if not (is_prime(p) and is_prime(q)):
        raise ValueError('Both numbers must be prime.')
    elif p == q:
        raise ValueError('p and q cannot be equal')

    # n = pq
    n:int = p * q

    # phi = (p-1)(q-1)
    phi:int = (p - 1) * (q - 1)

    # Choose an integer e such that e and phi(n) are coprime
    e = random.randrange(1, phi)

    # Use Euclid's Algorithm to verify that e and phi(n) are comprime
    g = gcd(e, phi)
    while g != 1:
        e:int = random.randrange(1, phi)
        g:int = gcd(e, phi)

    # Use Extended Euclid's Algorithm to generate the private key
    d = multiplicative_inverse(e, phi)
    # Return public and private keypair
    # Public key is (e, n) and private key is (d, n)
    return ((e, n), (d, n))
